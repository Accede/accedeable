<?php

namespace Accede\Behaviors\Accedeable;
use Propel\Generator\Model\Behavior;

class AccedeableBehavior extends Behavior {
	// default parameters value
	protected $parameters = array( );

	public function objectMethods() {
		$script = "";
		$script .= $this -> addJSONControls();
		return $script;
	}

	protected function addJSONControls() {

		$table = $this -> getTable();
		return $this -> renderTemplate('JSONControls', array('table' => $table));

	}

	

}
