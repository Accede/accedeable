/**
 * Read and Write JSON Functions for <?php echo $table->getName() ?>
 * uses to and from Array functions
 */


public function toJSON($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
{
  $arr = $this->toArray($keyType, $includeLazyLoadColumns, $alreadyDumpedObjects, $includeForeignObjects);
  return json_encode($arr);
  
}

public function fromJSON($JSON, $keyType = TableMap::TYPE_PHPNAME)
{
   $arr = json_decode($JSON);
   $this->fromArray($arr, $keyType);
   return $this;
   
}